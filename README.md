# TypeScript `wasm-bindgen`

This is just a super basic example showing `wasm-bindgen` working with TypeScript that I created while playing with, and learning about, the tool.

## Running the demo

Ensure you're using rust nightly:  
`rustup default nightly`

Also install the `wasm-bindgen` tools if you don't have them:  
`rustup target add wasm32-unknown-unknown`  
`cargo +nightly install wasm-bindgen-cli`

To build the `wasm` library and TypeScript definitions run:  
`npm run build:rust`

Install the demo dependancies:  
`npm i`

Then start the demo:  
`npm start`