#!/usr/bin/env node

const execSync = require('child_process').execSync;

const fs = require('fs');
const dir = './native/dist';

!fs.existsSync(dir) && fs.mkdirSync(dir);

execSync('cd native && cargo +nightly build --target wasm32-unknown-unknown', { stdio: [0,1,2] });

execSync('cd native && wasm-bindgen target/wasm32-unknown-unknown/debug/js_hello_world.wasm --out-dir ./dist', { stdio: [0,1,2] });
